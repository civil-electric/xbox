#!/usr/bin/python3
#

# X 120-205
# Y 0-120

import serial
import sys
import time
import atexit
import os
import re
import shutil
import argparse
import time
import threading
import queue
import types

from multiprocessing import Process,Queue

from gxs700 import usbint
from gxs700 import util
from gxs700 import img

tif_re = re.compile('.*\.TIF$')

application_shutdown_signal = threading.Event()

def wait_for_file(timeout):
    old_listing = list(filter(lambda x: tif_re.match(x), os.listdir('/mnt')))
    listing = old_listing
    count = 0

    while len(listing) <= len(old_listing):
        if count >= timeout:
            return False

        time.sleep(1)
        count += 1
        listing = list(filter(lambda x: tif_re.match(x), os.listdir('/mnt')))

    for x in listing:
        if x not in old_listing:
            fname = x

    return fname

def subdivide_interval(minimum, maximum, n_subdivisions):
    index = 0
    minimum = float(minimum)
    maximum = float(maximum)
    delta = (maximum-minimum) / n_subdivisions
    current = minimum

    while index < n_subdivisions:
        yield current
        current += delta
        index += 1


class XRayScanner(threading.Thread):

    def __init__(
        self, 
        serial_port,
        application_shutdown_signal
    ):
        threading.Thread.__init__(self, name="XraySupervisor")
 
        self.operation_queue = queue.Queue()
        self.operation_complete = threading.Event()
        self.operation_complete.clear()
        self.port = serial_port
        self.application_shutdown_signal = application_shutdown_signal


    def run(self):

        # Only run while the program is active
        while(not self.application_shutdown_signal.is_set()):

            try:
                # TODO: Use EOS check for faster shut-down
                operation = self.operation_queue.get(block=True, timeout=1)

                if(operation is not None):
                    
                    if(operation[0] == "home"):
                        self.home()
                        self.wait()

                    elif(operation[0] == "goto"):
                        self.goto(operation[1])
                        self.wait()

                    elif(operation[0] == "wait"):
                        self.wait()

                    elif(operation[0] == "xray_pulse"):
                        try:
                            time.sleep(operation[1][0])
                            self.xray_on()
                            time.sleep(operation[1][1])
                            self.xray_off()
                        except:
                            print("Error, proper format is '[xray_pulse, (delay_to_start, duration)]'")

                    elif(operation[0] == "xray_off"):
                        self.xray_off()

                    elif(operation[0] == "delay"):
                        self.delay(operation[1])

                    # Wait for operation to finish & unblock 
                    # threads waiting on operation queue
                    self.operation_complete.set()
                    self.operation_complete.clear()

            except queue.Empty:
                # Nothing to process
                pass

        # Cleanup
        self.xray_off()


    def home(self):
        self.port.write(b'G28 XY\n')
        self.get_response()

    def goto(self, xy):
        self.port.write(b'G1 X%d Y%d\n' % (int(xy[0]),int(xy[1])))
        self.get_response()

    def xray_on(self):
        #print("X-RAY ON!")
        self.port.write(b'M106\n')
        self.get_response()

    def xray_off(self):
        #print("X-RAY OFF!")
        self.port.write(b'M107\n')
        self.get_response()

    def delay(self, milliseconds):
        self.port.write(b'G4 P%d\n' % milliseconds)
        self.get_response()

    def wait(self):
        self.port.write(b'M400\n')
        self.get_response()

    def get_response(self, s = b'ok\n'):
        index = 0

        while index < len(s):
            x = self.port.read(1)

            if(x == b''):
                print("Error, no response from serial port")
            else:
                print(x.decode('utf-8'), end="")
                sys.stdout.flush()

            if s[index] == x[0]:
                index += 1

        self.port.flushInput()
        return True


def process_bin_queue(queue):
    while True:
        item = queue.get()
        if not item:
            break

        fp = open(item, 'r')
        imgdata = fp.read()
        fp.close()

        print("HOST: Decoding image...")
        img = gxs.decode(imgdata)

        print("HOST: Writing image...")
        img.save("%s.png" % item)


if __name__ == "__main__":

    print("██╗  ██╗██████╗  ██████╗ ██╗  ██╗")
    print("╚██╗██╔╝██╔══██╗██╔═══██╗╚██╗██╔╝")
    print(" ╚███╔╝ ██████╔╝██║   ██║ ╚███╔╝ ")
    print(" ██╔██╗ ██╔══██╗██║   ██║ ██╔██╗ ")
    print("██╔╝ ██╗██████╔╝╚██████╔╝██╔╝ ██╗")
    print("╚═╝  ╚═╝╚═════╝  ╚═════╝ ╚═╝  ╚═╝")
    print("")

    # =========================================================================
    # Parse arguments
    parser = argparse.ArgumentParser(description='X-Ray Stepper',
                                 formatter_class=argparse.RawTextHelpFormatter)

    # Commands
    parser.add_argument("--x_begin", dest="x_begin", help="begin x position")
    parser.add_argument("--y_begin", dest="y_begin", help="begin y position")
    parser.add_argument("--x_end", dest="x_end", help="end x position")
    parser.add_argument("--y_end", dest="y_end", help="end y position")
    parser.add_argument("--n_scans", dest="n_scans", help="total scans")
    parser.add_argument("--tty_port", dest="marlin_tty", help="serial port for marlin")
    parser.set_defaults(marlin_tty="/dev/ttyACM0")

    # Print info about commands
    parser.print_help()
    
    # Get the command inputs
    args = parser.parse_args()

    # Debug command inputs
    print(f'\nHOST: Using serial port {args.marlin_tty}\n')
    
    # Connect to serial port
    serial_port = serial.Serial(args.marlin_tty, 230400, timeout=10.0)
    
    # Create X-Ray Scanner Thread
    scanner = XRayScanner(
        serial_port,
        application_shutdown_signal
    )
    scanner.daemon = True

    # Start X-Ray Scanner Thread
    scanner.start()

    # X-Ray should be off
    #scanner.operation_queue.put(["home"])
    #scanner.operation_complete.wait()
    
    # Get limits of stage
    begin = (float(args.x_begin), float(args.y_begin))
    end = (float(args.x_end), float(args.y_end))
    n_subdivisions = int(args.n_scans)

    count = 0
    
    # Garbage collector
    def cleanup():
        application_shutdown_signal.set()
        serial_port.write(b'M107\n')

    # Register garbage collector
    atexit.register(cleanup)

    # Initialze the GXS700 stuff
    gxs = usbint.GXS700()
    gxs._init()
    bin_queue = Queue()
    Process(target=process_bin_queue,args=(bin_queue,),daemon=True).start()

    # Event for successful capture
    capture_success = threading.Event()

    # Callback for GXS700 cap_binv()
    outdir='/tmp'
    def cb(n, imgb):
        binfn = os.path.join(outdir, "cap_%02u.bin" % n)
        open(binfn, 'w').write(str(imgb))
        bin_queue.put(binfn)
        capture_success.set()
    
    # Wait for printer to boot
    print(f"\nHOST: Waiting for printer to wake up\n")
    scanner.operation_queue.put(["wait"])
    scanner.operation_complete.wait()

    # Home the printer
    print(f'\nHOST: Homing printer\n')
    scanner.operation_queue.put(["home"])
    scanner.operation_complete.wait()

    # TODO: Wait until capture device is ready
    time.sleep(1)

    # Begin the scan
    print(f'\nHOST: Beginning scan!\n')

    for y in subdivide_interval(begin[1], end[1], n_subdivisions):
        for x in subdivide_interval(begin[0], end[0], n_subdivisions):

            # Where are we?
            print(f"\nHOST: Starting sub-scan at ({x},{y})\n")
                        
            # Go to new position
            print(f"\nHOST: Going to new position: x={x}, y={y}\n")
            scanner.operation_queue.put(["goto", (x,y)])
            scanner.operation_complete.wait()

            # Wait for vibrations
            print(f"\nHOST: Sleep 1 second to stop bad vibes\n")
            time.sleep(1)

            # Start x-ray pulse
            xray_delay = 0.1
            xray_duration = 1
            print(f"\nHOST: Beam on for {xray_duration}s after {xray_delay}s\n")
            scanner.operation_queue.put(["xray_pulse", (xray_delay, xray_duration)])

            # Clear capture success flag
            capture_success.clear()

            # Start new capture
            print(f"\nHOST: Start new GXS capture\n")
            gxs.cap_binv(1, cb, xr=None)
            
            # Wait for xray pulse to finish
            print(f"\nHOST: Waiting x-ray to finish\n")
            scanner.operation_complete.wait()

            # Make sure xray is off
            print(f"\nHOST: Enforcing x-ray off\n")
            scanner.operation_queue.put(["xray_off"])
            scanner.operation_complete.wait()

            # Wait for GXS ready
            print(f"\nHOST: Waiting for GXS to finish scan\n")
            capture_success.wait()

            # Done
            print(f"\nHOST: Sub-scan complete\n")

    # Terminate x-ray machine
    application_shutdown_signal.set()

#                filename = wait_for_file(30)
#                if not filename:
#                    print ("No file! retrying...")
#                    continue
#
#                success = True
#                count += 1
#                new_filename = '/tmp/%d-%f-%f.tif' % (count, x, y)
#
#                print("copying '%s' -> '%s'" % ("/mnt/%s" % filename, new_filename))
#                shutil.copy("/mnt/%s" % filename, new_filename)
